#!/bin/sh
##
##  adns-config -- ADNS config tool
##  Copyright (c) 1999 Ralf S. Engelschall <rse@engelschall.com>
##
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
##
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  Lesser General Public License for more details.
##
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##

DIFS=' 
'

prefix="@prefix@"
exec_prefix="@exec_prefix@"

adns_prefix="$prefix"
adns_exec_prefix="$exec_prefix"
adns_bindir="@bindir@"
adns_libdir="@libdir@"
adns_includedir="@includedir@"
adns_mandir="@mandir@"
adns_datadir="@datadir@"
adns_acdir="@datadir@/aclocal"
adns_cflags="@CFLAGS@"
adns_ldflags="@LDFLAGS@"
adns_libs="@LIBS@"
adns_version="@ADNS_VERSION@"

help=no
version=no

usage="adns-config"
usage="$usage [--help] [--version] [--all]"
usage="$usage [--prefix] [--exec-prefix] [--bindir] [--libdir] [--includedir] [--mandir] [--datadir] [--acdir]"
usage="$usage [--cflags] [--ldflags] [--libs]"
if [ $# -eq 0 ]; then
    echo "adns-config:Error: Invalid option" 1>&2
    echo "adns-config:Usage: $usage" 1>&2
    exit 1
fi
output=""
output_extra=""
all=no
prev=''
OIFS="$IFS" IFS="$DIFS"
for option
do
    if [ ".$prev" != . ]; then
        eval "$prev=\$option"
        prev=""
        continue
    fi
    case "$option" in
        -*=*) optarg=`echo "$option" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
           *) optarg='' ;;
    esac
    case "$option" in
        --help|-h)
            echo "Usage: $usage"
            exit 0
            ;;
        --version|-v)
            echo "GNU adns $adns_version"
            exit 0
            ;;
        --all)
            all=yes
            ;;
        --prefix)
            output="$output $adns_prefix"
            ;;
        --exec-prefix)
            output="$output $adns_exec_prefix"
            ;;
        --bindir)
            output="$output $adns_bindir"
            ;;
        --libdir)
            output="$output $adns_libdir"
            ;;
        --includedir)
            output="$output $adns_includedir"
            ;;
        --mandir)
            output="$output $adns_mandir"
            ;;
        --datadir)
            output="$output $adns_datadir"
            ;;
        --acdir)
            output="$output $adns_acdir"
            ;;
        --cflags)
            output="$output -I$adns_includedir"
            output_extra="$output_extra $adns_cflags"
            ;;
        --ldflags)
            output="$output -L$adns_libdir"
            output_extra="$output_extra $adns_ldflags"
            ;;
        --libs)
            output="$output -ladns"
            output_extra="$output_extra $adns_libs"
            ;;
        * )
            echo "adns-config:Error: Invalid option" 1>&2
            echo "adns-config:Usage: $usage" 1>&2
            exit 1;
            ;;
    esac
done
IFS="$OIFS"
if [ ".$prev" != . ]; then
    echo "adns-config:Error: missing argument to --`echo $prev | sed 's/_/-/g'`" 1>&2
    exit 1
fi
if [ ".$output" != . ]; then
    if [ ".$all" = .yes ]; then
        output="$output $output_extra"
    fi
    echo $output
fi

